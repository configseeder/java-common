/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.utils;

import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

public final class RsaKeyUtil {

    private RsaKeyUtil() {
        // hidden c'tor
    }

    public static PublicKey getPublicKeyFromBase64String(String base64encodedPublicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] publicKey = Base64.getDecoder().decode(base64encodedPublicKey);
        return getPublicKey(publicKey);
    }

    public static PublicKey getPublicKeyFromPem(String pem) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String base64key = pem.replaceAll("(?m)^-----.*", "");
        byte[] publicKey = Base64.getMimeDecoder().decode(base64key);
        return getPublicKey(publicKey);
    }

    public static PublicKey getPublicKey(byte[] publicKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        X509EncodedKeySpec ks = new X509EncodedKeySpec(publicKey);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(ks);
    }

    public static String getPublicKeyAsString(PublicKey publicKey) {
        return Base64.getEncoder().encodeToString(publicKey.getEncoded());
    }

    public static String getPublicKeyAsPem(PublicKey publicKey) {
        return "-----BEGIN PUBLIC KEY-----\n"
                + Base64.getMimeEncoder().encodeToString(publicKey.getEncoded()) + "\n"
                + "-----END PUBLIC KEY-----\n";
    }

    public static PrivateKey getPrivateKeyFromBase64String(String base64EncodedPrivateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] privateKey = Base64.getDecoder().decode(base64EncodedPrivateKey);
        return getPrivateKey(privateKey);
    }

    public static PrivateKey getPrivateKeyFromPem(String pem) throws NoSuchAlgorithmException, InvalidKeySpecException {
        String base64key = pem.replaceAll("(?m)^-----.*", "");
        byte[] privateKey = Base64.getMimeDecoder().decode(base64key);
        return getPrivateKey(privateKey);
    }

    public static PrivateKey getPrivateKey(byte[] privateKey) throws NoSuchAlgorithmException, InvalidKeySpecException {
        PKCS8EncodedKeySpec keySpecPkcs8 = new PKCS8EncodedKeySpec(privateKey);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePrivate(keySpecPkcs8);
    }

    public static String getPrivateKeyAsString(PrivateKey privateKey) {
        return Base64.getEncoder().encodeToString(privateKey.getEncoded());
    }

    public static String getPrivateKeyAsPem(PrivateKey privateKey) {
        return "-----BEGIN PRIVATE KEY-----\n"
                + Base64.getMimeEncoder().encodeToString(privateKey.getEncoded()) + "\n"
                + "-----END PRIVATE KEY-----\n";
    }

    public static byte[] encrypt(Key privateKey, byte[] contents)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        // specify mode and padding
        Cipher encrypt = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        // init with the privateKey
        encrypt.init(Cipher.ENCRYPT_MODE, privateKey);
        // encrypt with known character encoding, you should probably use hybrid cryptography instead
        return encrypt.doFinal(contents);
    }

    public static byte[] decrypt(Key publicKey, byte[] contents)
            throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher decrypt = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        decrypt.init(Cipher.DECRYPT_MODE, publicKey);
        return decrypt.doFinal(contents);
    }

}
