/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class RsaKeyUtilTest {

    @Test
    void shouldSerializeAndDeserializeBas64String() throws Exception {
        // given
        final KeyPair keyPair = RsaKeyGenerator.generateKeyPair(2048);

        // when
        PrivateKey privateKey = RsaKeyUtil.getPrivateKeyFromBase64String(RsaKeyUtil.getPrivateKeyAsString(keyPair.getPrivate()));
        PublicKey publicKey = RsaKeyUtil.getPublicKeyFromBase64String(RsaKeyUtil.getPublicKeyAsString(keyPair.getPublic()));

        // then
        assertThat(privateKey).isEqualTo(keyPair.getPrivate());
        assertThat(publicKey).isEqualTo(keyPair.getPublic());
    }

    @Test
    void shouldSerializeAndDeserializePem() throws Exception {
        // given
        final KeyPair keyPair = RsaKeyGenerator.generateKeyPair(2048);

        // when
        PrivateKey privateKey = RsaKeyUtil.getPrivateKeyFromPem(RsaKeyUtil.getPrivateKeyAsPem(keyPair.getPrivate()));
        PublicKey publicKey = RsaKeyUtil.getPublicKeyFromPem(RsaKeyUtil.getPublicKeyAsPem(keyPair.getPublic()));

        // then
        assertThat(privateKey).isEqualTo(keyPair.getPrivate());
        assertThat(publicKey).isEqualTo(keyPair.getPublic());
    }

    @Test
    void shouldBeAbleToEncryptAndDecrypt() throws Exception {
        // given
        final KeyPair keyPair = RsaKeyGenerator.generateKeyPair(2048);
        final String privateEncoded = RsaKeyUtil.getPrivateKeyAsPem(keyPair.getPrivate());
        final String publicEncoded = RsaKeyUtil.getPublicKeyAsPem(keyPair.getPublic());
        final String contentToEncrypt = "some-dynamic-content-" + UUID.randomUUID().toString();

        // when
        final PrivateKey privateKey = RsaKeyUtil.getPrivateKeyFromPem(privateEncoded);
        final PublicKey publicKey = RsaKeyUtil.getPublicKeyFromPem(publicEncoded);

        final byte[] encrypted = RsaKeyUtil.encrypt(privateKey, contentToEncrypt.getBytes(Charset.defaultCharset()));
        final byte[] decrypt = RsaKeyUtil.decrypt(publicKey, encrypted);

        // then
        assertThat(new String(decrypt, Charset.defaultCharset())).isEqualTo(contentToEncrypt);
    }

    @Test
    void generatedPemShouldMatch() throws Exception {
        final String privateKey = "-----BEGIN PRIVATE KEY-----\n"
                + "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAN8T61E0RXePE4JsyjmKnlqb7Rja\n"
                + "VLdSk4BqLzYVPPYFq7ALWAcJRwAMWrvi9TbCpJDGCKEJ0F9ceo6359zg10l2h7Di6SIcrb7vIX9i\n"
                + "acw2FpyShpyj9kCQaXf94soLemq2X3o/jSCD9i6auvPZIwh8h8wQqShKWrvRBv9OeFFRAgMBAAEC\n"
                + "gYA5PhJHPyOIP3WhJStrQo/J7nMTyoqslAr3i8fbl+2Rr/jTbsXC4HWQpJrv1ZhY2e+Cmt6B/FO0\n"
                + "9LUaRETCIAv9+jsp5vq5xb099MiPQYB9Ow2df0zFKBl9a/ANTIzG8s1Twm+ckxS/kNTHT4g7833E\n"
                + "g/dkyiRiht3fJ8VCMh+ScQJBAPNutkLhv0d1rRMNE7Xz4TmBLwEzQI6vhT5mMYUHpTGhl5xV99Q6\n"
                + "+HdQ/PiBwS9McGUwr7pGMwZoxhuqb0JDDkUCQQDqmDFoznzHzr+kPC6CCfKLSJc3334rL+esOwB/\n"
                + "23ikn86dV6mfURS8+s3cBASHEuvpTujM7El5N/AsPpb3292dAkEAiiHUgnce1ynLH/hXqvPMZZkM\n"
                + "Vx4CD/VBIntSti30awlKwod1rZQY0Xu/UDQwruotDP5neMfExGpHoANCRtNOEQJAeDdlmn+2Ee4w\n"
                + "5siwTJMsZ5PbahZtYVQ7NFdH+j1NpY7/IkI5+hpitjUQKR8l8tIPco+tXNX5ENVJjmLEml9hQQJA\n"
                + "GbyV4AUHFx4ve4qX+D5HWmmGh69yULOVmBvTdB7DwqrHymvJ0C7P6OUcdOqhMc1pldd2/MAYD+3R\n"
                + "2/6yrCvGMA==\n"
                + "-----END PRIVATE KEY-----\n";
        final String publicKey = "-----BEGIN PUBLIC KEY-----\n"
                + "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDfE+tRNEV3jxOCbMo5ip5am+0Y2lS3UpOAai82\n"
                + "FTz2BauwC1gHCUcADFq74vU2wqSQxgihCdBfXHqOt+fc4NdJdoew4ukiHK2+7yF/YmnMNhackoac\n"
                + "o/ZAkGl3/eLKC3pqtl96P40gg/Yumrrz2SMIfIfMEKkoSlq70Qb/TnhRUQIDAQAB\n"
                + "-----END PUBLIC KEY-----\n";

        assertThat(RsaKeyUtil.getPublicKeyAsPem(RsaKeyUtil.getPublicKeyFromPem(publicKey))).isEqualToIgnoringWhitespace(publicKey);
        assertThat(RsaKeyUtil.getPrivateKeyAsPem(RsaKeyUtil.getPrivateKeyFromPem(privateKey))).isEqualToIgnoringWhitespace(privateKey);
    }

    @Test
    void shouldStillBeAbleToDecryptAfterChangeOfDecryptionMethod() throws Exception {
        // given
        // String privateKeyEncoded = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCZpOiXloNcxESaYRBoQmz38Lq3UnBS8F14Z8tdgM3YrUlo5b4P0r4Ie2O1dWIp8vjJaUhm/Buyv4z0BQHJT73UH5nQsd4kVJiPLU7NCN6gKFCYDJpHeEr7r2E2mkUpd9HNNgK8BQnnHT2dtk8F+BOaRTXxMve1agUXi5+0wf1QUR2hx55xETvsAIT+s0YiOnl4+yoHoMRZIt2nm6etc649SYxGbtN1yz3EQNJ4+pCZkNBjhNu4yNX3MLvE8acAmejLA8dezPPy9qBGIaN0N45uJCFohsj3ZeW4J43kqkFyeKE6wob6dnIC8fze1oDAz9gR+wNmNf6NXitN1KWqPe2BAgMBAAECggEBAIwjXgLlxetXGTu2TMDL1ZhQef27s7fyhU7NXTBry1VWuR50bmcAUVRrNwbLij244DLVVLSaPqlLDcWEy5xSn1JbgHBZy8RXZM8zBCz/ag+qtdf5pr6JedsWnBSxoWOxZitGmDfmYKHsicbQAkPvgfG0Lv9Q/Z76lKplL8YvKl2WNRQptentzE2qfKoQfOuZjqKdIj7TXWd8dQl1DMx3eEzq67vTvaXshBaHukclScFBik/hpFwJF5uAdAzDjavAmNnrznN78Rn2tZfWkGOoXAACH1vWkU5oNUVkGpFhTBkXP9BDEFSayzmqcwVWQa/h9+dz9PNchdahp/qvHCrzoAECgYEAylxxCVmbRn3CkNTzz8HIhOoggRU9vNk0CbzyI86hnLLdiFNw6iNkpLAc9kpoyhqOC79h2GYR5HxgxIuBWv/D0xKJP0BcXWAFQlNXoDb9oWxlLa9fNukSe5VB6fF9/hG3qsvTW+pkz9Bbo3qsUUbdHeTgz1WDZrVWtFRA24qpFnkCgYEAwl6yW/8wG4xubGaKHzue7kU16Tz72b+vZBAIQUKSL2XRL6M74jkBAThE8/bLQwq/kQmiygxaWpLUSNi4ix2N+r8JbwrvVcuCSz97c13fwG5uPDqRczMU6AW8HbDcYHScZupDLJsfM6bYfNF8qAy1KSRq8DJV+thof5/Z29PdbUkCgYBPD8DmG0lMSDQtK9Y6FIcITwT2Cdt+Hgpzx4t/K+r+cu+UpK0X/U4D/f7Cxx2b4LYgptxOlMpmz6VrBOo8KHNU0wgmWzn7DVngqPkyUmnTmKsLJ1p29KbQ++atxTkpe7iblVj1r2VFwC0deP88Hgij9llTCCD3jt/aQpS4cbtQWQKBgHxIMsGyVV94Fu9HTZAU/lWaeZNOhDdCYvKXcb/Qwkp+TxinxlWrX2IZSbW5G1Ud3UnvIGETLv71HboKoU2Ks0setfiDxG9WdlSJHfWxk62mruPiCzje88I8UIGnofIm/g6sQhGWiofrake9Z3jIz1mkSKCXfvSrRX49hv8R5GbJAoGBAIDS39b6VfzFt1M400S8r/VgzDTJ1kblnuimN75b+LCNe2UNZNQoyFkSpXACXIHfzfSXFKdt1RObORWG1QolTWqHbMkbZGfGZcGhPVKa36XLRTnpfwQyWG1oJKKowGZZODk/meaib5clX3KudFI4i1EdA1p66FdEqeDIWZ6cw6L+";
        // final PrivateKey privateKey = RsaKeyUtil.getPrivateKeyFromBase64String(privateKeyEncoded);
        // String encryptedContent = new String(Base64.getEncoder().encode(RsaKeyUtil.encrypt(privateKey, "hello-world".getBytes(StandardCharsets.UTF_8))), StandardCharsets.UTF_8);
        String publicKeyEncoded = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAmaTol5aDXMREmmEQaEJs9/C6t1JwUvBdeGfLXYDN2K1JaOW+D9K+CHtjtXViKfL4yWlIZvwbsr+M9AUByU+91B+Z0LHeJFSYjy1OzQjeoChQmAyaR3hK+69hNppFKXfRzTYCvAUJ5x09nbZPBfgTmkU18TL3tWoFF4uftMH9UFEdoceecRE77ACE/rNGIjp5ePsqB6DEWSLdp5unrXOuPUmMRm7Tdcs9xEDSePqQmZDQY4TbuMjV9zC7xPGnAJnoywPHXszz8vagRiGjdDeObiQhaIbI92XluCeN5KpBcnihOsKG+nZyAvH83taAwM/YEfsDZjX+jV4rTdSlqj3tgQIDAQAB";
        String encryptedContent = "XLur/MRWd97FUUmrJz+uhG19YIzxY/V3hA4uwGOkMzjuFSi2T2+Ie53TkZ8JO4ZFXogcLr19F/2fhijjJnMDXs44lYTRV4zVMWfbbZKRzNChPXN+jR4HBLqF+svKuGyXOD/2mKlGWvhG7RGHa6KZqhMszcsnioIenPWGG1XYvDJhckP6knk49SXlLtpmSMle+JErfbcuS7Nbx9tmyjZ+1n00BWbaiUqxWjy+GEsS/zNlVPTOHsc9irZ1xjlNu2IUwOXG/6x6ZyClLb26DM0taTP7YDgd6GU/sglydNELpFgLXnGP91GD4C31Tbw5tgUKOGnQdZcjzsnrC0GXiZaRRw==";

        final PublicKey publicKey = RsaKeyUtil.getPublicKeyFromBase64String(publicKeyEncoded);
        final byte[] decrypt = RsaKeyUtil.decrypt(publicKey, Base64.getDecoder().decode(encryptedContent.getBytes(StandardCharsets.UTF_8)));

        // then
        assertThat(new String(decrypt, Charset.defaultCharset())).isEqualTo("hello-world");
    }

}