/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.shared.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.InputStream;
import java.util.Properties;
import org.junit.jupiter.api.Test;

class YamlToPropertiesConverterTest {

    @Test
    void shouldBeAbleToReadExample1() {
        final YamlToPropertiesConverter yamlToPropertiesConverter = new YamlToPropertiesConverter();

        final InputStream example1 = YamlToPropertiesConverterTest.class.getResourceAsStream("/read-example-1.yaml");

        final Properties readProperties = yamlToPropertiesConverter.convert(example1);

        assertThat(readProperties.get("base.0.foo")).isEqualTo("blafoo");
        assertThat(readProperties.get("base.0.bar")).isEqualTo("blabar");
        assertThat(readProperties.get("base.1.foo")).isEqualTo("nixfoo");
        assertThat(readProperties.get("base.1.bar")).isEqualTo("nixbar");
    }

    @Test
    void shouldBeAbleToReadExample2() {
        final YamlToPropertiesConverter yamlToPropertiesConverter = new YamlToPropertiesConverter();

        final InputStream example2 = YamlToPropertiesConverterTest.class.getResourceAsStream("/read-example-2.yaml");

        final Properties readProperties = yamlToPropertiesConverter.convert(example2);

        assertThat(readProperties.get("bar")).isEqualTo("hello");
        assertThat(readProperties.get("foo")).isEqualTo("world");
    }

    @Test
    void shouldBeAbleToReadExample3() {
        final YamlToPropertiesConverter yamlToPropertiesConverter = new YamlToPropertiesConverter();

        final InputStream example3 = YamlToPropertiesConverterTest.class.getResourceAsStream("/read-example-3.yaml");

        final Properties readProperties = yamlToPropertiesConverter.convert(example3);

        assertThat(readProperties.get("level1.level2.simple")).isEqualTo("value");
        assertThat(readProperties.get("level1.level2.indented.0.1.a")).isEqualTo("b");
        assertThat(readProperties.get("level1.level2.indented.0.1.c")).isEqualTo("d");
        assertThat(readProperties.get("level1.level2.indented.1")).isEqualTo(2);
        assertThat(readProperties.get("level1.level2.indented.2")).isEqualTo(3);
        assertThat(readProperties.get("level1.level2.indented.3.[with.dot]")).isEqualTo("has a dot");
        assertThat(readProperties.get("level1.level2.indented.4.multiline")).isEqualTo("this is a\nmultiline");
    }

}