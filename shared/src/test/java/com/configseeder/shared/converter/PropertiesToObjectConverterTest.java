/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.shared.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TreeMap;
import java.util.function.Supplier;
import lombok.RequiredArgsConstructor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PropertiesToObjectConverterTest {

    private final PropertiesToObjectConverter converter = new PropertiesToObjectConverter();

    @Test
    void shouldTokenizeAsExpected() {
        final List<ObjectPath> path = converter.getParts("prefix.[concatenated.sub.key].infix.[1].and.0");

        assertThat(path.size()).isEqualTo(6);
        assertThat(path.get(0).getKey()).isEqualTo("prefix");
        assertThat(path.get(1).getKey()).isEqualTo("concatenated.sub.key");
        assertThat(path.get(2).getKey()).isEqualTo("infix");
        assertThat(path.get(3).getKey()).isEqualTo(1);
        assertThat(path.get(4).getKey()).isEqualTo("and");
        assertThat(path.get(5).getKey()).isEqualTo(0);
    }

    @Test
    void shouldFailIfThereAreSubElementsAndAValueForTheSameKey() {
        // given
        final Properties properties = new Properties();
        properties.put("welcome", "Welcome!");
        properties.put("welcome.de", "Grüäzi");

        // when
        Assertions.assertThrows(IllegalStateException.class, () -> converter.convertPropertiesToObject(properties));
    }

    @Test
    void shouldExportProperties() {
        // given
        final Properties properties = new Properties();
        properties.put("hello.world", "Welcome!");
        properties.put("hello.my.[0].switzerland", "Grüäzi");
        properties.put("i18n.[0]", "deutsch");
        properties.put("i18n.[1]", "français");
        properties.put("i18n.[2].v1.a", "V1a");
        properties.put("i18n.2.v1.b", "V1b");
        properties.put("i18n.2.v2", "V2");
        properties.put("logger.[com.configseeder.core].level", "ERROR");

        // when
        final Object rootObject = converter.convertPropertiesToObject(properties);

        // then
        assertThat(rootObject).isOfAnyClassIn(TreeMap.class);
        assertThat(((Map) ((Map) rootObject).get("hello"))).hasSize(2);
        assertThat(((List) ((Map) rootObject).get("i18n"))).hasSize(3);
        assertThat(((Map) ((Map) rootObject).get("logger"))).hasSize(1);
    }

    @Test
    void shouldConvertNumberAndCharacterOnSameLevel() {
        Properties properties = new Properties();
        properties.put("1", "1");
        properties.put("a", "a");

        // when
        final Object rootObject = converter.convertPropertiesToObject(properties);
        assertThat(rootObject).isOfAnyClassIn(TreeMap.class);
        assertThat((String) ((Map) rootObject).get("1")).isEqualTo("1");
        assertThat((String) ((Map) rootObject).get("a")).isEqualTo("a");
    }

    @Test
    void shouldConvertObjectWithToString() {
        // given
        Properties properties = new Properties();
        properties.put(1, "should be ignored");
        properties.put("with.toString", new ToString(() -> "Lazy Hello"));

        // when
        Object result = converter.convertPropertiesToObject(properties);

        // then
        assertThat(result).isInstanceOf(Map.class);
        assertThat(((Map) result).get("with")).isInstanceOf(Map.class);
        assertThat(((Map) ((Map) result).get("with")).get("toString")).isInstanceOf(ToString.class);
        assertThat(((Map) ((Map) result).get("with")).get("toString").toString()).isEqualTo("Lazy Hello");
    }


    @RequiredArgsConstructor
    private static class ToString {
        private final Supplier<String> stringSupplier;
        @Override
        public String toString() {
            return stringSupplier.get();
        }
    }
}