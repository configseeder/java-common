/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.shared.converter;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Properties;
import org.junit.jupiter.api.Test;

class PropertiesToYamlConverterTest {

    private final PropertiesToYamlConverter converter = new PropertiesToYamlConverter();

    @Test
    void shouldExportPropertiesEverythingSortedAlphabetically() {
        // given
        final Properties properties = new Properties();
        properties.put("server.port", "8080");
        properties.put("server.name", "https://localhost");
        properties.put("my.key.two", "value2");
        properties.put("my.key.one", "value1");

        // when
        final String string = converter.convert(properties);

        // then
        String result = "my:\n"
                + "  key:\n"
                + "    one: value1\n"
                + "    two: value2\n"
                + "server:\n"
                + "  name: https://localhost\n"
                + "  port: '8080'\n";

        assertThat(string).isEqualTo(result);
    }

    @Test
    void shouldExportProperties() {
        // given
        final Properties properties = new Properties();
        properties.put("hello.world", "Welcome!");
        properties.put("hello.my.[0].switzerland", "Grüäzi");
        properties.put("i18n.[0]", "deutsch");
        properties.put("i18n.[1]", "français");
        properties.put("i18n.[2].v1.a", "V1a");
        properties.put("i18n.2.v1.b", "V1b");
        properties.put("i18n.2.v2", "V2");
        properties.put("logger.[com.configseeder.core].level", "ERROR");

        // when
        final String string = converter.convert(properties);

        // then
        assertThat(string).contains("hello:\n"
                                            + "  my:\n"
                                            + "  - switzerland: Grüäzi\n"
                                            + "  world: Welcome!\n");
        assertThat(string).contains("i18n:\n"
                                            + "- deutsch\n"
                                            + "- français\n"
                                            + "- v1:\n"
                                            + "    a: V1a\n"
                                            + "    b: V1b\n"
                                            + "  v2: V2\n");
        assertThat(string).contains("logger:\n"
                                            + "  com.configseeder.core:\n"
                                            + "    level: ERROR");
    }

}