/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.PrivateKey;
import java.security.PublicKey;

/**
 * Migrates a public key / private key encoded as base64 to a pem structure.
 */
public class RsaKeyMigrator {

    /**
     * Generates public and private key.
     *
     * @param args command line args
     */
    public static void main(String[] args) throws Exception {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            println("Please enter your private key (encoded as base64 - not pem!)");
            String base64privateKey = br.readLine();
            PrivateKey privateKey = RsaKeyUtil.getPrivateKeyFromBase64String(base64privateKey);
            println("Please enter you public key (encoded as base64 - not pem!");
            String base64publicKey = br.readLine();
            PublicKey publicKey = RsaKeyUtil.getPublicKeyFromBase64String(base64publicKey);

            println("Thank you. Here follows the private key and public key as pem:");
            println("Private Key (e.g. used for 'configseeder.server.security.auth.api-key.credential.private-key-location')");
            println(RsaKeyUtil.getPrivateKeyAsPem(privateKey));
            println("Public Key  (e.g. used for 'configseeder.server.security.auth.api-key.credential.public-key-location')");
            println(RsaKeyUtil.getPublicKeyAsPem(publicKey));
        }
    }

    private static void println(String content) {
        System.out.println(content); // NOSONAR
    }

}