/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.shared.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class ComparableVersionTest {

    @Test
    void shouldBeEqual() {
        assertThat(cv("1.15")).isEqualTo(cv("1.15.0"));
        assertThat(cv("1.15-alpha")).isEqualTo(cv("1.15.0-alpha"));
        assertThat(cv("1.15.0-m1")).isEqualTo(cv("1.15.0-milestone1"));
        assertThat(cv("1.15.0.")).isEqualTo(cv("1.15.0"));
        assertThat(cv("1.15.0-")).isEqualTo(cv("1.15.0"));
    }

    @Test
    void shouldCompareQualifiers() {
        assertThat(cv("1.15.0-SNAPSHOT")).isLessThan(cv("1.15.0"));
        assertThat(cv("1.15.0-SNAPSHOT")).isLessThan(cv("1.15.0.SNAPSHOT"));
        assertThat(cv("1.15.SNAPSHOT")).isLessThan(cv("1.15.aa"));
        assertThat(cv("1.15.alpha")).isLessThan(cv("1.15.SNAPSHOT"));
        assertThat(cv("1.15.alpha")).isLessThan(cv("1.15.0"));
        assertThat(cv("1.15.alpha")).isLessThan(cv("1.15.beta"));
        assertThat(cv("1.15.0-alpha")).isLessThan(cv("1.15.0"));
        assertThat(cv("1.15.0")).isLessThan(cv("1.15.0-RTM"));
    }

    @Test
    void qualifiersShouldBeExposed() {
        assertThat(ComparableVersion.QUALIFIERS).containsExactly("alpha", "beta", "milestone", "rc", "snapshot", "", "sp");
    }

    @Test
    void shouldCompareOrder() {
        // given
        final ComparableVersion oneFiveZero = cv("1.5.0");
        final ComparableVersion oneTenZero = cv("1.10.0");

        // then
        assertThat(oneFiveZero).isLessThan(oneTenZero);
    }

    @Test
    void shouldIgnoreZeroOnVersions() {
        // given
        final ComparableVersion oneDotOFiveZero = cv("1.05.0");
        final ComparableVersion oneFiveZero = cv("1.5.0");

        // then
        assertThat(oneDotOFiveZero).isEqualTo(oneFiveZero);
    }

    @Test
    void shouldSupportRcAndSnapshot() {
        // given
        final ComparableVersion twoOneSnapshot = cv("2.1.0-SNAPSHOT");
        final ComparableVersion releaseCandidate = cv("2.1.0-RC1");

        // then
        assertThat(releaseCandidate).isLessThan(twoOneSnapshot);
    }

    private ComparableVersion cv(String version) {
        return new ComparableVersion(version);
    }

}

