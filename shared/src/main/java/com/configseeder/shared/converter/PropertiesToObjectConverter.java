/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.shared.converter;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.stream.Collectors;
import lombok.Getter;

/**
 * Converts a property file to a YAML file.
 */
public class PropertiesToObjectConverter {

    /**
     * Converts properties to list, map, objects.
     *
     * @param properties properties file (key -> value)
     * @return String | Map<String, Map,List,String> | List<Map,List,String>
     */
    public Object convertPropertiesToObject(Properties properties) {
        final List<PropertyKey> propertyKeys = properties.keySet().stream()
                .filter(k -> k instanceof String)
                .map(k -> new PropertyKey((String) k))
                .collect(toList());
        return map(properties, 0, propertyKeys);
    }

    /**
     * Export all keys on the given level, called recursively.
     *
     * @param properties Property source
     * @param level level to export the values. It's expected that all propertyKeys until this level have the same common path
     * @param propertyKeys all property key.
     */
    private Object map(Properties properties, int level, List<PropertyKey> propertyKeys) {
        if (propertyKeys.size() == 1) {
            return mapSingle(properties, level, propertyKeys);
        } else {
            return mapMultiple(properties, level, propertyKeys);
        }
    }

    private Object mapSingle(Properties properties, int level, List<PropertyKey> propertyKeys) {
        final PropertyKey propertyKey = propertyKeys.get(0);
        // has children
        final boolean hasSubPath = propertyKey.paths.length > level;
        if (hasSubPath) {
            final int nextLevel = level + 1;
            final ObjectPath child = propertyKey.paths[level];
            if (StringObjectPath.class.equals(child.getClass())) {
                return singletonMap(((StringObjectPath) child).getPath(), map(properties, nextLevel, propertyKeys));
            } else if (IndexObjectPath.class.equals(child.getClass())) {
                return singletonList(map(properties, nextLevel, propertyKeys));
            } else {
                throw new IllegalStateException("Not supported class " + child.getClass());
            }
        } else {
            return properties.get(propertyKey.propertyName);
        }
    }

    private Object mapMultiple(Properties properties, int level, List<PropertyKey> propertyKeys) {
        // Split up children by type
        final int nextLevel = level + 1;
        final List<PropertyKey> endPath = propertyKeys.stream()
                .filter(e -> e.paths.length == level)
                .collect(toList());
        final Map<ObjectPath, List<PropertyKey>> groupedPathElement = propertyKeys.stream()
                .filter(e -> e.paths.length > level)
                .sorted(Comparator.comparing(propertyKey -> propertyKey.paths[level]))
                .collect(Collectors.groupingBy(propertyKey -> propertyKey.paths[level],
                                               TreeMap::new,
                                               Collectors.mapping(propertyKey -> propertyKey, Collectors.toList())));
        if (!endPath.isEmpty()) {
            throw new IllegalStateException("There are multiple keys registered for the same key. Path: "
                                                    + propertyKeys.get(0).propertyName + ", Level: " + level + ", Count: " + propertyKeys.size());
        }
        // Check if mixed situation. If so, expose array with index as key
        final Map<? extends Class<? extends ObjectPath>, Long> numberByClass = groupedPathElement.keySet()
                .stream()
                .collect(groupingBy(ObjectPath::getClass, Collectors.counting()));
        // Use string as key
        if (numberByClass.size() > 1 || numberByClass.get(StringObjectPath.class) != null) {
            return groupedPathElement.entrySet().stream()
                    .collect(toMap(e -> e.getKey().getStringKey(),
                                   e -> map(properties, nextLevel, e.getValue()),
                                   (v1, v2) -> {
                                       throw new IllegalStateException("There are multiple keys registered for the same key. Key: "
                                                                               + v1 + ", Level: " + level + ", Count: " + propertyKeys.size());
                                   },
                                   TreeMap::new));
        } else {
            // They are all IndexYamlPath
            return groupedPathElement.values().stream()
                    .map(e -> map(properties, nextLevel, e))
                    .collect(toList());
        }
    }

    /**
     * Split up a property key into path elements of type string or index.
     *
     * @param propertyKey the property key
     * @return all path elements
     */
    List<ObjectPath> getParts(String propertyKey) {
        final List<ObjectPath> pathSegments = new LinkedList<>();
        final StringTokenizer tokenizer = new StringTokenizer(propertyKey.trim(), ".[]", true);
        while (tokenizer.hasMoreTokens()) {
            final String token = tokenizer.nextToken(".[]");
            if (token.equals(".") || token.equals("]")) {
                continue;
            }
            if (token.equals("[")) {
                final String part = tokenizer.nextToken("]");
                if (!part.equals("]")) {
                    pathSegments.add(ObjectPath.of(part, true));
                }
            } else {
                pathSegments.add(ObjectPath.of(token, false));
            }
        }
        return pathSegments;
    }

    @Getter
    private class PropertyKey {

        private final ObjectPath[] paths;
        private final String propertyName;

        private PropertyKey(String propertyName) {
            this.propertyName = propertyName;
            this.paths = getParts(propertyName).toArray(new ObjectPath[0]);
        }
    }
}
