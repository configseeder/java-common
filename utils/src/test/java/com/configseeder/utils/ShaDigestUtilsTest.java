/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.Charset;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Test;

class ShaDigestUtilsTest {

    @Test
    void shouldSha512HexString() {
        // given
        String hashMe = "someRandomText123";

        // when
        final String result = ShaDigestUtils.shaHex(512, hashMe);

        // then
        assertThat(result).isEqualTo("0b2a5252985ff3977493b707f4310c6855ad9d1db9fe3b8ab719879e59fcb7082b4100bec9235a246875ba8c557615a7417fe64f282ca986daf162366a643993");
        assertThat(result).isEqualTo(DigestUtils.sha512Hex(hashMe));
    }

    @Test
    void shouldSha256HexString() {
        // given
        String hashMe = "someRandomText123";

        // when
        final String result = ShaDigestUtils.shaHex(256, hashMe);

        // then
        assertThat(result).isEqualTo("d3d060d69c9d901052fbcea0c798bc888511713cc8945296608cba29db9e02c6");
        assertThat(result).isEqualTo(DigestUtils.sha256Hex(hashMe));
    }

    @Test
    void shouldSha1HexString() {
        // given
        String hashMe = "someRandomText123";

        // when
        final String result = ShaDigestUtils.shaHex(1, hashMe);

        // then
        assertThat(result).isEqualTo("646421640422045e2e227744526f704d3a54670b");
        assertThat(result).isEqualTo(DigestUtils.sha1Hex(hashMe));
    }

    @Test
    void shouldSha1SaltedHexString() {
        // given
        String hashMe = "someRandomText123";
        String salt = "anySalt";

        // when
        final String result = ShaDigestUtils.shaHex(1, salt.getBytes(Charset.defaultCharset()), hashMe);

        // then
        assertThat(result).isEqualTo("2af259fa9a9a659a0b4b2dc1d0e397bae1b81005");
    }

    @Test
    void shouldSha1HexWithByte() {
        // given
        String hashMe = "someRandomText123";

        // when
        final String result = ShaDigestUtils.shaHex(1, hashMe.getBytes(Charset.defaultCharset()));

        // then
        assertThat(result).isEqualTo("646421640422045e2e227744526f704d3a54670b");
    }

    @Test
    void shouldSha1SaltHexWithByte() {
        // given
        String hashMe = "someRandomText123";
        String salt = "anySalt";

        // when
        final String result = ShaDigestUtils.shaHex(1, salt.getBytes(Charset.defaultCharset()), hashMe.getBytes(Charset.defaultCharset()));

        // then
        assertThat(result).isEqualTo("2af259fa9a9a659a0b4b2dc1d0e397bae1b81005");
    }


}
