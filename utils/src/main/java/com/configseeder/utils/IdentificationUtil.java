/*
 * Copyright (c) 2019 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.utils;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Helper util to identify the current instance.
 */
public final class IdentificationUtil {

    private IdentificationUtil() {
        // hidden c'tor.
    }

    /**
     * Details about the current operation system.
     * @return os type ; version ; architecture
     */
    public static String getOperatingSystemDetails() {
        final String osName = System.getProperty("os.name");
        final String osVersion = System.getProperty("os.version");
        final String architecture = System.getProperty("os.arch");
        return String.format("%s; %s; %s", osName, osVersion, architecture);
    }

    /**
     * Hostname of the current host. First detected by network and then by environment.
     *
     * @return may be null
     */
    public static String getHostname() {
        try {
            final InetAddress ip = InetAddress.getLocalHost();
            return ip.getHostName();
        } catch (UnknownHostException e) {
            return findEnv(null, "HOSTNAME", "COMPUTERNAME");
        }
    }

    /**
     * Current user based on environment. May be null.
     *
     * @return may be null
     */
    public static String getUser() {
        return findEnv(null, "USER", "USERNAME", "LOGNAME");
    }

    /**
     * Identification based on mac address.
     *
     * @return identification based on mac address
     */
    public static String getIdentificationBasedOnHardware() {
        try {
            final Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            final String basedOnInterfaces = Collections.list(networkInterfaces).stream()
                    .map(n -> {
                        try {
                            final byte[] macAddress = n.getHardwareAddress();
                            if (macAddress == null) {
                                return null;
                            }
                            final StringBuilder sb = new StringBuilder();
                            for (int i = 0; i < macAddress.length; i++) {
                                sb.append(String.format("%02X%s", macAddress[i], (i < macAddress.length - 1) ? ":" : ""));
                            }
                            return sb.toString();
                        } catch (SocketException e) {
                            return null;
                        }
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.joining(","));
            if (basedOnInterfaces != null) {
                return basedOnInterfaces;
            }
        } catch (IOException e) {
            return null;
        }
        return null;
    }

    private static String findEnv(String fallback, String... keys) {
        for (String key : keys) {
            final String value = System.getenv(key);
            if (value != null) {
                return value;
            }
        }
        return fallback;
    }

}
