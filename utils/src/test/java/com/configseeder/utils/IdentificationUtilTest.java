/*
 * Copyright (c) 2019 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.utils;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class IdentificationUtilTest {

    @Test
    void shouldGetOperatingSystemDetails() {
        // given
        System.setProperty("os.name", "Linux");
        System.setProperty("os.version", "4.9.0-4-amd64");
        System.setProperty("os.arch", "amd64");

        // when
        final String details = IdentificationUtil.getOperatingSystemDetails();

        // then
        assertThat(details).isEqualTo("Linux; 4.9.0-4-amd64; amd64");
    }

    @Test
    void shouldGetHostnameFromEnvironment() {
        // given

        // when
        final String hostname = IdentificationUtil.getHostname();

        // then
        assertThat(hostname).isNotEmpty();
    }

    @Test
    void shouldGetUserName() {
        // given

        // when
        final String user = IdentificationUtil.getUser();

        // then
        // assertThat(user).isNotEmpty();
    }

    @Test
    void shouldGetIdentification() {
        // when
        final String macAddresses = IdentificationUtil.getIdentificationBasedOnHardware();

        // then
        assertThat(macAddresses).isNotEmpty();
    }



}