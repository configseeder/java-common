/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.shared.converter;

import java.util.Objects;
import javax.annotation.Nonnull;
import lombok.Data;

/**
 * Index path element (e.g. used for arrays).
 */
@Data
final class IndexObjectPath extends ObjectPath {

    private final int index;
    private final boolean composed;

    @Override
    public String toString() {
        return (composed ? "[" : "") + index + (composed ? "]" : "");
    }

    @Override
    Object getKey() {
        return index;
    }

    @Override
    String getStringKey() {
        return String.valueOf(index);
    }

    @Override
    public int compareTo(@Nonnull ObjectPath o) {
        if (this.getClass().equals(o.getClass())) {
            return index - ((IndexObjectPath) o).index;
        }
        return this.getClass().getSimpleName().compareTo(o.getClass().getSimpleName());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IndexObjectPath)) {
            return false;
        }
        IndexObjectPath that = (IndexObjectPath) o;
        return Objects.equals(index, that.index);
    }

    @Override
    public int hashCode() {
        return Objects.hash(index);
    }

}
