/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.Base64;
import javax.crypto.spec.SecretKeySpec;

public class AesKeyGenerator {

    /**
     * Generate secret key.
     */
    public static void main(String[] args) throws Exception {
        // Get desired key from the user
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            println("Do you want to generate a new random key? [Y/n]");
            String shouldGenerateRandomKey = br.readLine();
            byte[] key;
            if ("Y".equals(shouldGenerateRandomKey) || "".equals(shouldGenerateRandomKey)) {
                key = generateRandomKey(br);
            } else {
                key = readManualKey(br);
            }

            // Generate the secret key
            SecretKeySpec secretKey = new SecretKeySpec(key, "AES");

            // Secret that should be inserted in property file.
            String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());

            // Done
            String configurationKey = "configseeder.server.security.configurationvalue.encryption.secret";
            String envConfigurationKey = configurationKey.toUpperCase().replace('.', '_');
            println("Put following line into `" + configurationKey + "` or as env parameter `" + envConfigurationKey + "`:");
            println(encodedKey);
        }
    }

    static byte[] readManualKey(BufferedReader br) throws IOException {
        byte[] key;
        println("Please provide your own string (16 characters)");
        do {
            String desiredKey = br.readLine();
            key = desiredKey.getBytes(StandardCharsets.UTF_8);
            if (key.length != 16) {
                println("Key has byte length of " + key.length + ". Should be 16");
            } else {
                break;
            }
        } while (true);
        return key;
    }

    static byte[] generateRandomKey(BufferedReader br) throws IOException {
        byte[] key;
        int length = 16;
        /*
        println("How long should the random key be? 16 (128bit), 24 (192bit) or 32 (256bit)");
        do {
            print("Length [16]: ");
            String keyLength = br.readLine();
            if ("".equals(keyLength)) {
                length = 16;
            } else {
                try {
                    length = Integer.valueOf(keyLength);
                } catch (NumberFormatException e) {
                    length = -1;
                }
            }
        } while (length != 16 && length != 24 && length != 32);
         */
        SecureRandom secureRandom = new SecureRandom();
        key = new byte[length];
        secureRandom.nextBytes(key);
        return key;
    }

    private static void println(String content) {
        System.out.println(content); // NOSONAR
    }

    private static void print(String content) {
        System.out.print(content); // NOSONAR
    }

}