/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Starter {

    /**
     * Starter.
     *
     * @param args not used
     */
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        println("Welcome to config server utils. What do you want to do?");
        println("[1] Generate AES encryption key");
        println("[2] Generate RSA private / public key");
        println("[3] Convert public and private key to PEM");
        int selection;
        do {
            System.out.print("Selection: ");
            String answer = br.readLine();
            try {
                selection = Integer.parseInt(answer);
            } catch (NumberFormatException e) {
                selection = -1;
            }
        } while (selection <= 0 || selection > 3);

        switch (selection) {
            case 1:
                AesKeyGenerator.main(args);
                break;
            case 3:
                RsaKeyMigrator.main(args);
                break;
            default:
            case 2:
                RsaKeyGenerator.main(args);
                break;
        }
    }

    private static void println(String content) {
        System.out.println(content); // NOSONAR
    }

}
