/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Provides SHA512 digest methods.
 *
 * <p>
 * Based on Commons Codec, which does not presently provide SHA512 support.
 * </p>
 *
 * @author Ben Alex
 * @since 2.0.1
 */
public final class ShaDigestUtils {

    private ShaDigestUtils() {

    }

    /**
     * Returns an SHA digest.
     *
     * @return An SHA digest instance.
     *
     * @throws RuntimeException when a {@link java.security.NoSuchAlgorithmException} is caught.
     */
    private static MessageDigest getShaDigest(int size) {
        try {
            return MessageDigest.getInstance("SHA-" + size);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * Calculates the SHA digest and returns the value as a <code>byte[]</code>.
     *
     * @param size SHA-{size}
     * @param data Data to digest
     * @return SHA digest
     */
    public static byte[] sha(int size, byte[] data) {
        return getShaDigest(size).digest(data);
    }

    /**
     * Calculates the SHA digest and returns the value as a <code>byte[]</code>.
     *
     * @param size SHA-{size}
     * @param salt update of the digest
     * @param data Data to digest
     * @return SHA digest
     */
    public static byte[] sha(int size, byte[] salt, byte[] data) {
        final MessageDigest shaDigest = getShaDigest(size);
        shaDigest.update(salt);
        return shaDigest.digest(data);
    }

    /**
     * Calculates the SHA digest and returns the value as a <code>byte[]</code>.
     *
     * @param size SHA-{size}
     * @param data Data to digest
     * @return SHA digest
     */
    public static byte[] sha(int size, String data) {
        return sha(size, data.getBytes());
    }

    /**
     * Calculates the SHA digest and returns the value as a <code>byte[]</code>.
     *
     * @param size SHA-{size}
     * @param salt update of the digest
     * @param data Data to digest
     * @return SHA digest
     */
    public static byte[] sha(int size, byte[] salt, String data) {
        return sha(size, salt, data.getBytes());
    }

    /**
     * Calculates the SHA digest and returns the value as a hex string.
     *
     * @param size SHA-{size}
     * @param data Data to digest
     * @return SHA digest as a hex string
     */
    public static String shaHex(int size, byte[] data) {
        return new String(Hex.encode(sha(size, data)));
    }

    /**
     * Calculates the SHA digest and returns the value as a hex string.
     *
     * @param size SHA-{size}
     * @param salt update of the digest
     * @param data Data to digest
     * @return SHA digest as a hex string
     */
    public static String shaHex(int size, byte[] salt, byte[] data) {
        return new String(Hex.encode(sha(size, salt, data)));
    }

    /**
     * Calculates the SHA digest and returns the value as a hex string.
     *
     * @param size SHA-{size}
     * @param data Data to digest
     * @return SHA digest as a hex string
     */
    public static String shaHex(int size, String data) {
        return new String(Hex.encode(sha(size, data)));
    }

    /**
     * Calculates the SHA digest and returns the value as a hex string.
     *
     * @param size SHA-{size}
     * @param salt update of the digest
     * @param data Data to digest
     * @return SHA digest as a hex string
     */
    public static String shaHex(int size, byte[] salt, String data) {
        return new String(Hex.encode(sha(size, salt, data)));
    }

}
