/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.shared.converter;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.yaml.snakeyaml.Yaml;

/**
 * Extracts from a yaml input stream all properties and put's it into a properties object.
 */
public class YamlToPropertiesConverter {

    /**
     * Extracts from a yaml input stream all properties and put's it into a properties object.
     *
     * @param yamlInputStream input stream of a yaml
     * @return extracted properties
     */
    public Properties convert(InputStream yamlInputStream) {
        final Yaml yaml = new Yaml();
        final Object load = yaml.load(yamlInputStream);

        final Properties properties = new Properties();
        extractRedirect(null, properties, load);
        return properties;
    }

    @SuppressWarnings("unchecked")
    private void extractRedirect(String path, Properties properties, Object object) {
        if (object == null) {
            return;
        }
        if (object instanceof Map) {
            extract(path, properties, (Map<Object, Object>) object);
        } else if (object instanceof List) {
            extract(path, properties, (List<Object>) object);
        } else {
            extract(path, properties, object);
        }
    }


    private void extract(String path, Properties properties, Object value) {
        properties.put(path, value);
    }

    private void extract(String path, Properties properties, Map<Object, Object> map) {
        String basePath = (path == null || "".equals(path)) ? "" : path + ".";
        map.forEach((k, v) -> extractRedirect(basePath + escapeKey(String.valueOf(k)), properties, v));
    }

    private void extract(String path, Properties properties, List<Object> list) {
        final Object[] entries = list.toArray();
        final String basePath = path == null ? "" : path + ".";
        for (int i = 0; i < entries.length; i++) {
            extractRedirect(basePath + i, properties, entries[i]);
        }
    }

    private String escapeKey(String key) {
        if (key != null && key.contains(".")) {
            return "[" + key + "]";
        }
        return key;
    }

}
