/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

/**
 * Public and private key generator used for JWT and License management.
 */
public class RsaKeyGenerator {

    /**
     * Generates public and private key.
     *
     * @param args command line args
     */
    public static void main(String[] args) throws Exception {
        // Get desired key from the user
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            println("How strong should your key be?");
            println("[1] 2048");
            println("[2] 4096 (default)");

            int keySize = 0;
            do {
                String keyStrength = br.readLine();
                if ("1".equals(keyStrength)) {
                    keySize = 2048;
                } else if ("2".equals(keyStrength) || "".equals(keyStrength)) {
                    keySize = 4096;
                } else {
                    printErrln("Invalid selection. Should be 1 or 2");
                }
            } while (keySize == 0);

            println("Key with strength of " + keySize + " will be generated. Please wait ...");

            KeyPair kp = generateKeyPair(keySize);
            String publicEncodedString = RsaKeyUtil.getPublicKeyAsPem(kp.getPublic());
            String privateEncodedString = RsaKeyUtil.getPrivateKeyAsPem(kp.getPrivate());

            println("Private Key (e.g. used for 'configseeder.server.security.auth.api-key.credential.private-key-location')");
            println(privateEncodedString);
            println("Public Key  (e.g. used for 'configseeder.server.security.auth.api-key.credential.public-key-location')");
            println(publicEncodedString);
        }
    }

    static KeyPair generateKeyPair(int keySize) throws NoSuchAlgorithmException {
        KeyPairGenerator keyGenerator = KeyPairGenerator.getInstance("RSA");
        keyGenerator.initialize(keySize);
        return keyGenerator.genKeyPair();
    }

    private static void println(String content) {
        System.out.println(content); // NOSONAR
    }

    private static void printErrln(String content) {
        System.err.println(content); // NOSONAR
    }

}