/*
 * Copyright (c) 2020 Oneo GmbH (ConfigSeeder®, https://configseeder.com/) and others.
 *
 * Any form of reproduction, distribution, exploitation or alteration is prohibited
 * without the prior written consent of Oneo GmbH.
 */

package com.configseeder.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import org.junit.jupiter.api.Test;

class RsaKeyGeneratorTest {

    @Test
    void shouldGenerateKey() throws NoSuchAlgorithmException {
        // given
        final KeyPair keyPair = RsaKeyGenerator.generateKeyPair(2048);
        assertThat(RsaKeyUtil.getPrivateKeyAsString(keyPair.getPrivate()).length()).isGreaterThan(800);
        assertThat(RsaKeyUtil.getPublicKeyAsString(keyPair.getPublic()).length()).isGreaterThan(200);
    }

}
