# ConfigSeeder :: Java :: Common

Shared implementations among all different projects.

## shared

- Support for Properties to YAML File conversion
- Support for YAML to Properties File conversion

## utils

Start with `java -jar configseeder-utils-<version>-jar-with-dependencies.jar`.
You will get following options:

```
[1] Generate AES encryption key
[2] Generate RSA private / public key
[3] Convert public and private key to PEM
```