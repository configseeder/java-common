/*
 * Copyright (c) 2021 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.shared.converter;

/**
 * Generic implementation of a YAML path element.
 */
abstract class ObjectPath implements Comparable<ObjectPath> {

    /**
     * Returns path key.
     *
     * @return key which represents the path element
     */
    abstract Object getKey();

    /**
     * Returns path key.
     *
     * @return key which represents the path element
     */
    abstract String getStringKey();

    /**
     * Simplified c'tor.
     *
     * @param path path
     * @param composed {@code true} if it's part of a composed element (noted with [ and ])
     * @return typed path object
     */
    static ObjectPath of(String path, boolean composed) {
        try {
            int index = Integer.parseInt(path);
            return ObjectPath.ofIndex(index, composed);
        } catch (NumberFormatException e) {
            return ObjectPath.ofPath(path, composed);
        }
    }

    private static ObjectPath ofPath(String path, boolean composed) {
        return new StringObjectPath(path, composed);
    }

    private static ObjectPath ofIndex(int index, boolean composed) {
        return new IndexObjectPath(index, composed);
    }

}
