/*
 * Copyright 2002-2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.utils;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

/**
 * Test cases for {@link Hex}.
 *
 * @author Kazuki Shimizu
 */
class HexTest {

    @Test
    void encode() {
        assertThat(Hex.encode(new byte[] { (byte) 'A', (byte) 'B', (byte) 'C',
                                           (byte) 'D' })).isEqualTo(new char[] {'4', '1', '4', '2', '4', '3', '4', '4'});
    }

    @Test
    void encodeEmptyByteArray() {
        assertThat(Hex.encode(new byte[] {})).isEmpty();
    }

    @Test
    void decode() {
        assertThat(Hex.decode("41424344")).isEqualTo(new byte[] { (byte) 'A', (byte) 'B', (byte) 'C',
                                                                  (byte) 'D' });
    }

    @Test
    void decodeEmptyString() {
        assertThat(Hex.decode("")).isEmpty();
    }

    @Test
    void decodeNotEven() {
        assertThrows(IllegalArgumentException.class, () -> Hex.decode("414243444"), "Hex-encoded string must have an even number of characters");
    }

    @Test
    void decodeExistNonHexCharAtFirst() {
        assertThrows(IllegalArgumentException.class, () -> Hex.decode("GO"), "Detected a Non-hex character at 1 or 2 position");
    }

    @Test
    void decodeExistNonHexCharAtSecond() {
        assertThrows(IllegalArgumentException.class, () -> Hex.decode("410G"), "Detected a Non-hex character at 3 or 4 position");
    }

    @Test
    void decodeExistNonHexCharAtBoth() {
        assertThrows(IllegalArgumentException.class, () -> Hex.decode("4142GG"), "Detected a Non-hex character at 5 or 6 position");
    }

}