/*
 * Copyright (c) 2019 Oneo GmbH (ConfigSeeder, https://configseeder.com/) and others.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.configseeder.utils;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.Map;
import lombok.Data;
import org.junit.jupiter.api.Test;


class GroupByWithNullTest {

    @Test
    void shouldCollectWithNullValues() {
        final List<KeyValue> keyValues = asList(new KeyValue("prod", "prod1"), new KeyValue("prod", "prod2"), new KeyValue("test", "test1"), new KeyValue(null, "default"));

        final Map<String, List<KeyValue>> envMap = keyValues.stream().collect(GroupByWithNull.groupingByWithNullKeys(KeyValue::getKey));

        assertThat(envMap.values()).hasSize(3);
    }

    @Data
    private static final class KeyValue {
        private final String key;
        private final String value;
    }

}
